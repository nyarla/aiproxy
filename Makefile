APPNAME = aiproxy
USER 	= nyarla
HOST 	= s13.rs2.gehirn.jp
BOPS 	= /Users/nyarla/web/Bops/bin/bops

SSHHOST = $(USER)@$(HOST)
SSHHOME = /home/$(USER)

APPDIR	= $(SSHHOME)/Applications/$(APPNAME)
MANAGER = $(SSHHOME)/System/supervisord/bin/supervisorctl

install: sync setup restart

update:	sync update-deps restart

sync:
	rsync -avz --exclude-from .rsyncignore . $(SSHHOST):$(APPDIR)/
	$(BOPS) $(SSHHOST) exec /bin/chmod +x $(APPDIR)/run

setup:
	$(BOPS) $(SSHHOST) exec /bin/bash $(APPDIR)/Setupfile

update-deps:
	$(BOPS) $(SSHHOST) exec /bin/bash $(APPDIR)/Updatefile

restart:
	$(BOPS) $(SSHHOST) exec $(MANAGER) reread aiproxy
	$(BOPS) $(SSHHOST) exec $(MANAGER) restart aiproxy


