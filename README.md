aiproxy - Amazon Image URL Proxy
================================

This script is the redirect proxy for Amazon's item images.

Usage
-----

This script is optimized with my server environments.
So, it's hard to write how to use. X<

License
-------

  * License of `proxy.psgi` is same as Perl.

Author
------

  * Naoki OKAMURA (Nyarla) <nyarla@thotep.net>


