#!/usr/bin/env perl

use strict;
use warnings;

use Plack::Request;

use LWP::UserAgent::WithCache;
use URI::Amazon::APA;

use File::Spec;
use File::Basename;

my $APP_ROOT        = File::Spec->rel2abs( File::Basename::dirname(__FILE__) );
my $CACHE_ROOT      = File::Spec->catfile( $APP_ROOT, '.cache' );
my $DOTENV          = File::Spec->catfile( $APP_ROOT, '.env.pl' );
my $secrets         = require $DOTENV;

if ( ref($secrets) ne 'HASH' ) {
    die "Failed to load .env.pl: $!";
}

my $AIP_KEY         = $secrets->{'AIP_KEY'};
my $AIP_SECRET      = $secrets->{'AIP_SECRET'};
my $AIP_ID          = $secrets->{'AIP_ID'};

my $app = sub {
    my $env         = shift;

    my $req     = Plack::Request->new($env);

    if ( $AIP_KEY eq q{} || $AIP_SECRET eq q{} || $AIP_ID eq q{} ) {
        my $ret = $req->new_response(500);
           $ret->content_type('text/plain');
           $ret->body('ENV values is nof found.');
        return $ret->finalize();
    }

    my $asin    = $req->param('asin') || return $req->new_response(400)->finalize;
    my $size    = $req->param('size') || 160;

    my $uri     = URI::Amazon::APA->new('http://webservices.amazon.co.jp/onca/xml');
       $uri->query_form(
            Service         => 'AWSECommerceService',
            Operation       => 'ItemLookup',
            ResponseGroup   => 'Images',
            IdType          => 'ASIN',
            ItemId          => $asin,
            AssociateTag    => $AIP_ID,
       );
       $uri->sign(
            key             => $AIP_KEY,
            secret          => $AIP_SECRET,
       );

    my $agent = LWP::UserAgent::WithCache->new(
        namespace           => 'aiproxy',
        cache_root          => $CACHE_ROOT,
        default_expire_in   => '24h',
    );

    my $res = $agent->get($uri);

    if ( ! $res->is_success ) {
        return $req->new_response(500)->finalize();
    } else {
        my ( $url ) = ( $res->content =~ m{<URL>(.+?)</URL>} );
        $url =~ s<_SL\d+_><_SL${size}_>;

        my $ret = $req->new_response(303);
           $ret->redirect( $url, 303 );
        return $ret->finalize;
    }
};

$app;
